/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef event_H
#define event_H

#ifndef wimp_H
#include "wimp.h"
#endif

#ifndef toolbox_H
#include "toolbox.h"
#endif

#ifndef bool
#include <stdbool.h>
#endif

#define event_ANY ((toolbox_o) -1)

typedef bool event_wimp_handler (wimp_event_no event_code,
      wimp_block *block, toolbox_block *id_block,
      void *handle);

typedef bool event_toolbox_handler (bits event_code,
      toolbox_action *action, toolbox_block *id_block,
      void *handle);

typedef bool event_message_handler (wimp_message *message,
      void *handle);

typedef struct event_wimp_handler_item event_wimp_handler_item;

struct event_wimp_handler_item
   {  event_wimp_handler_item *next;
      toolbox_o               object_id;
      wimp_event_no                     event_code;
      event_wimp_handler      *handler;
      void                    *handle;
   };


typedef struct event_toolbox_handler_item event_toolbox_handler_item;

struct event_toolbox_handler_item
   {  event_toolbox_handler_item *next;
      toolbox_o                  object_id;
      bits                        event_code;
      event_toolbox_handler      *handler;
      void                       *handle;
   };

typedef struct event_message_handler_item event_message_handler_item;

struct event_message_handler_item
   {  event_message_handler_item *next;
      int                        msg_no;
      event_message_handler      *handler;
      void                       *handle;
   };

extern void event_poll (wimp_event_no *event_code, wimp_block *poll_block,
      void *poll_word);

extern void event_poll_idle (wimp_event_no *event_code, wimp_block *poll_block,
      os_t earliest, void *poll_word);

extern void event_register_wimp_handler (toolbox_o object_id,
      wimp_event_no event_code, event_wimp_handler *handler, void *handle);

extern void event_deregister_wimp_handler (toolbox_o object_id,
      wimp_event_no event_code, event_wimp_handler *handler, void *handle);

extern void event_register_toolbox_handler (toolbox_o object_id,
      bits event_code, event_toolbox_handler *handler, void *handle);

extern void event_deregister_toolbox_handler (toolbox_o object_id,
      bits event_code, event_toolbox_handler *handler, void *handle);

extern void event_register_message_handler (bits msg_no,
      event_message_handler *handler, void *handle);

extern void event_deregister_message_handler (bits msg_no,
      event_message_handler *handler, void *handle);

extern void event_set_mask (wimp_poll_flags mask);

extern void event_get_mask (wimp_poll_flags *mask);

extern void event_initialise (toolbox_block *id_block);

#endif
